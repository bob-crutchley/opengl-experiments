#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>

#include "map.h"

#define PI 3.1415926535
#define PI_90_DEGREES PI/2
#define PI_270_DEGREES 3*PI/2
#define ONE_DEGREE_RADIANS 0.0174533

typedef struct {
  float positionX;
  float positionY;
  float deltaX;
  float deltaY;
  float angle;
  float moveSpeed;
  float strafingSpeed;
  float lookSpeed;
} Player;

typedef struct {
  int w, a, d, s;
} ButtonKeys; 

typedef struct {
  int positionX, positionY; 
} Mouse;

typedef struct {
  int width, height, centerX, centerY;
} Game;

Game game;
Mouse mouse;
ButtonKeys buttonKeys;

Player player;

float
fixAngle(float angle)
{
  if(angle>359) {
    angle-=360;
  }
  if(angle<0) {
    angle+=360;
  }
  return angle;
}

void
calculatePlayerDeltas()
{
  player.deltaX = cos(player.angle) * player.moveSpeed; 
  player.deltaY = sin(player.angle) * player.moveSpeed;
}

void
init()
{
  glClearColor(0.3,0.3,0.3,0);
  gluOrtho2D(0,game.width,game.height,0);
  player.positionX = 300;
  player.positionY = 300;
  mouse.positionX = 300;
  mouse.positionY = 300;
  player.moveSpeed = 8;
  player.strafingSpeed = 0.2;
  player.lookSpeed = 0.002;
  calculatePlayerDeltas();
}

void
drawPlayer()
{
  glColor3f(1,1,0);
  glPointSize(8);
  glBegin(GL_POINTS);
  glVertex2i(player.positionX, player.positionY);
  glEnd();

  /* Player line of sight. */
  glLineWidth(3);
  glBegin(GL_LINES);
  glVertex2i(player.positionX, player.positionY);
  glVertex2i(
    player.positionX + player.deltaX * player.moveSpeed,
    player.positionY + player.deltaY * player.moveSpeed
  );
  /* Left of Player. */
  glVertex2i(player.positionX, player.positionY);
  glVertex2i(
    player.positionX + player.deltaY * player.moveSpeed,
    player.positionY + -player.deltaX * player.moveSpeed
  );
  /* Right of Player. */
  glVertex2i(player.positionX, player.positionY);
  glVertex2i(
    player.positionX + -player.deltaY * player.moveSpeed,
    player.positionY + player.deltaX * player.moveSpeed
  );
  glEnd();
}

void 
drawMap2D()
{
  int x,y,xOffset,yOffset;
  int outline = 1;
  for (y = 0; y < mapY; y++) {
    for(x = 0; x < mapX; x++) {
      enum Block block = map[y * mapX + x];
      if(block == WALL) {
        glColor3f(1,1,1);
      }
      if (block == FLOOR) {
        glColor3f(0,0,0);
      }
      if (block == EMPTY) {
        glColor3f(255,0,0);
      }
      xOffset = x * mapSize;
      yOffset = y * mapSize;
      glBegin(GL_QUADS);
      glVertex2i(xOffset + outline, yOffset + outline);
      glVertex2i(xOffset + outline, yOffset + mapSize - outline);
      glVertex2i(xOffset + mapSize - outline, yOffset + mapSize - outline);
      glVertex2i(xOffset + mapSize - outline, yOffset + outline);
      glEnd();
    }
  }
}

float
distance(
  float firstPositionX, float firstPositionY,
  float secondPositionX, float secondPositionY, float angle
) {
  return (
    sqrt(
      (secondPositionX - firstPositionX) * (secondPositionX - firstPositionX) + 
      (secondPositionY - firstPositionY) * (secondPositionY - firstPositionY)
    )
  );
}

void
drawRays2D()
{
  int rayIndex, mapPositionX, mapPositionY, mapPosition,
      depthOfField, distanceTotal;
  float rayAngle, rayPositionX, rayPositionY, xOffset, yOffset;
  rayAngle = player.angle - ONE_DEGREE_RADIANS * 30;
  if (rayAngle < 0) {
    rayAngle += 2 * PI;
  }
  if (rayAngle > 2 * PI) {
    rayAngle -= 2 * PI;
  }
  float aTan, nTan;
  for(int rayIndex = 0; rayIndex < 60; rayIndex++) {
    /* Horizontal lines */
    depthOfField = 0;
    float horizontalRayDistance = 999999999;
    float horizontalRayX = player.positionX;
    float horizontalRayY = player.positionY;

    aTan = -1/tan(rayAngle);
    if (rayAngle > PI) { /* Looking up. */
      rayPositionY = (((int) player.positionY >> 6) << 6) - 0.0001;
      rayPositionX = (
        player.positionY - rayPositionY
      ) * aTan + player.positionX;
      yOffset = -64;
      xOffset = -yOffset * aTan;
    }
    if (rayAngle < PI) { /* Looking down. */
      rayPositionY = (((int) player.positionY >> 6) << 6) + 64;
      rayPositionX = (
        player.positionY - rayPositionY
      ) * aTan + player.positionX;
      yOffset = 64;
      xOffset = -yOffset * aTan;
    }
    /* Looking straight, left or right. */
    if (rayAngle == 0 || rayAngle == PI) { 
      rayPositionX = player.positionX; 
      rayPositionY = player.positionY; 
      depthOfField = 8;
    }
    /* TODO: The '8' here is just the width or length of the map. */
    while(depthOfField < 8) {
      mapPositionX = (int) (rayPositionX) >> 6;
      mapPositionY = (int) (rayPositionY) >> 6;
      mapPosition = mapPositionY * mapX + mapPositionX;
      if (
        mapPosition > 0 &&
        mapPosition < mapX * mapY &&
        map[mapPosition] == WALL
      ) {
        horizontalRayX = rayPositionX;
        horizontalRayY = rayPositionY;
        horizontalRayDistance = distance(
          player.positionX, player.positionY, horizontalRayX, horizontalRayY,
          rayAngle
        );
        depthOfField = 8; 
      } else {
        rayPositionX += xOffset;
        rayPositionY += yOffset;
        depthOfField += 1;
      }
    }

    /* Vertical lines */
    depthOfField = 0;
    float verticalRayDistance = 999999999;
    float verticalRayX = player.positionX;
    float verticalRayY = player.positionY;
    nTan = -tan(rayAngle);
    /* Looking left. */
    if (rayAngle > PI_90_DEGREES && rayAngle < PI_270_DEGREES) {
      rayPositionX = (((int) player.positionX >> 6) << 6) - 0.0001;
      rayPositionY = (
        player.positionX - rayPositionX
      ) * nTan + player.positionY;
      xOffset = -64;
      yOffset = -xOffset * nTan;
    }
    /* Looking right. */
    if (rayAngle < PI_90_DEGREES || rayAngle > PI_270_DEGREES) {
      rayPositionX = (((int) player.positionX >> 6) << 6) + 64;
      rayPositionY = (
        player.positionX - rayPositionX
      ) * nTan + player.positionY;
      xOffset = 64;
      yOffset = -xOffset * nTan;
    }
    /* Looking straight, up or down. */
    if (rayAngle == 0 || rayAngle == PI) { 
      rayPositionX = player.positionX; 
      rayPositionY = player.positionY; 
      depthOfField = 8;
    }
    /* TODO: The '8' here is just the width or length of the map. */
    while(depthOfField < 8) {
      mapPositionX = (int) (rayPositionX) >> 6;
      mapPositionY = (int) (rayPositionY) >> 6;
      mapPosition = mapPositionY * mapX + mapPositionX;
      if (
        mapPosition > 0 &&
        mapPosition < mapX * mapY &&
        map[mapPosition] == WALL
      ) {
        verticalRayX = rayPositionX;
        verticalRayY = rayPositionY;
        verticalRayDistance = distance(
          player.positionX, player.positionY, verticalRayX, verticalRayY,
          rayAngle
        );
        depthOfField = 8; 
      } else {
        rayPositionX += xOffset;
        rayPositionY += yOffset;
        depthOfField += 1;
      }
    }
    if (verticalRayDistance < horizontalRayDistance) {
      rayPositionX = verticalRayX;
      rayPositionY = verticalRayY;
      distanceTotal = verticalRayDistance;
      glColor3f(0.9, 0, 0);
    }
    if (horizontalRayDistance < verticalRayDistance) {
      rayPositionX = horizontalRayX;
      rayPositionY = horizontalRayY;
      distanceTotal = horizontalRayDistance;
      glColor3f(0.7, 0, 0);
    }
    glLineWidth(1);
    glBegin(GL_LINES);
    glVertex2i(player.positionX, player.positionY);
    glVertex2i(rayPositionX, rayPositionY);
    glEnd();

    /* Draw 3D Walls */
    float fisheEyeCorrectionAngle = player.angle - rayAngle;
    if (fisheEyeCorrectionAngle < 0) {
      fisheEyeCorrectionAngle += 2 * PI;
    }
    if (fisheEyeCorrectionAngle > 2 * PI) {
      fisheEyeCorrectionAngle -= 2 * PI;
    }
    distanceTotal = distanceTotal * cos(fisheEyeCorrectionAngle);
    float lineHeight = (mapSize * 320) / distanceTotal;
    int playerViewHeight = 300;
    float lineOffset = playerViewHeight - lineHeight / 2;
    if (lineOffset < 140) {
      lineOffset = 140;
    }
    if (lineHeight > 320 || horizontalRayDistance < 60) {
      lineHeight = 320;
    }
    glLineWidth(8);
    glBegin(GL_LINES);
    glVertex2i(rayIndex * 8 + 530, lineOffset);
    glVertex2i(rayIndex * 8 + 530, lineHeight + lineOffset);
    glEnd(); 
    rayAngle += ONE_DEGREE_RADIANS;
    if (rayAngle < 0) {
      rayAngle += 2 * PI;
    }
    if (rayAngle > 2 * PI) {
      rayAngle -= 2 * PI;
    }
  }
}

void
playerLookLeft(int multiplier)
{
  player.angle -= (player.lookSpeed * multiplier);
  if (player.angle < 0) {
    player.angle += 2 * PI; 
  }
  calculatePlayerDeltas();
}

void
playerLookRight(int multiplier)
{
  player.angle += (player.lookSpeed * multiplier);
  if (player.angle > 2 * PI) {
    player.angle -= 2 * PI; 
  }
  calculatePlayerDeltas();
}

void
display()
{
  int xOffset = 0;
  if (player.deltaX < 0) {
    xOffset = -20;
  } else {
    xOffset = 20;
  }
  int yOffset;
  if (player.deltaY < 0) {
    yOffset = -20;
  } else {
    yOffset = 20;
  }
  int playerGridPositionX = player.positionX / 64.0;
  int gridPositionXAddedOffset = (player.positionX + xOffset) / 64.0;
  int gridPositionXSubtractedOffset = (player.positionX - xOffset) / 64.0;
  int playerGridPositionY = player.positionY / 64.0;
  int gridPositionYAddedOffset = (player.positionY + yOffset) / 64.0;
  int gridPositionYSubtractedOffset = (player.positionY - yOffset) / 64.0;
  if (buttonKeys.w == 1) {
    if (map[playerGridPositionY * mapX + gridPositionXAddedOffset] == FLOOR) {
      player.positionX += player.deltaX * 0.2;
    }
    if (map[gridPositionYAddedOffset * mapX + playerGridPositionX] == FLOOR) {
      player.positionY += player.deltaY * 0.2;
    }
  }
  if (buttonKeys.s == 1) {
 	if (
      map[playerGridPositionY * mapX + gridPositionXSubtractedOffset] == FLOOR
    ) {
      player.positionX -= player.deltaX * 0.2;
    }
    if (
      map[gridPositionYSubtractedOffset * mapX + playerGridPositionX] == FLOOR
    ) {
      player.positionY -= player.deltaY * 0.2;
    }
  }
  /* TODO: Apply some logic to prevent the player from strafing off the map. */
  if (buttonKeys.a == 1) {
    player.positionX += player.deltaY * player.strafingSpeed;
    player.positionY += -player.deltaX * player.strafingSpeed;
  }
  if (buttonKeys.d == 1) {
    player.positionX += -player.deltaY * player.strafingSpeed;
    player.positionY += player.deltaX * player.strafingSpeed;
  }
  player.angle = fixAngle(player.angle);
  glutPostRedisplay();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  drawMap2D();
  drawRays2D();
  drawPlayer();
  glutSwapBuffers();
}

void
buttonUp(unsigned char key, int x, int y)
{
  switch(key) {
    case 'a': buttonKeys.a = 0; break;
    case 'd': buttonKeys.d = 0; break;
    case 'w': buttonKeys.w = 0; break;
    case 's': buttonKeys.s = 0; break;
  }
  glutPostRedisplay();
}

void
buttonDown(unsigned char key, int x, int y)
{
  switch(key) {
    case 'a': buttonKeys.a = 1; break;
    case 'd': buttonKeys.d = 1; break;
    case 'w': buttonKeys.w = 1; break;
    case 's': buttonKeys.s = 1; break;
  }
  glutPostRedisplay();
}

void
handleWindowResize(int width, int height)
{
  game.width = width;
  game.height = height;
  game.centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
  game.centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;
  // glutReshapeWindow(game.width, game.height);
}

void
handleMouse(int x, int y)
{
  if (x > game.centerX) {
    playerLookRight(x - game.centerX);
    glutWarpPointer(game.centerX, game.centerY);
  }
  if (x < game.centerX) {
    playerLookLeft(game.centerX - x);
    glutWarpPointer(game.centerX, game.centerY);
  }
}

int
main(int argc, char* argv[])
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  game.width = 1024;
  game.height = 512;
  glutInitWindowSize(game.width, game.height);
  glutInitWindowPosition(200, 200);
  glutCreateWindow("Raycaster");
  init();
  glutDisplayFunc(display);
  glutReshapeFunc(handleWindowResize);
  glutKeyboardFunc(buttonDown);
  glutKeyboardUpFunc(buttonUp);
  glutPassiveMotionFunc(handleMouse);
  glutSetCursor(GLUT_CURSOR_NONE);
  glutMainLoop();
  return 0;
}

