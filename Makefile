.PHONY: raycaster

raycaster:
	gcc raycaster.c -o raycaster -lglut -lGLU -lGL -lm
	./raycaster

raycaster-win:
	gcc raycaster.c -o raycaster.exe -lglu32 -lglut32 -lopengl32 -static-libgcc
	./raycaster.exe

